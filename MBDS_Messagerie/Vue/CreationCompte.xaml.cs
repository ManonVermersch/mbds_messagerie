﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MBDS_Messagerie.Vue
{
    /// <summary>
    /// Logique d'interaction pour CreationCompte.xaml
    /// </summary>
    public partial class CreationCompte : Window
    {
        public CreationCompte()
        {
            InitializeComponent();
        }

        private void Click_ValiderNewUser(object sender, RoutedEventArgs e)
        {

        }

        private void Click_AnnulerCreation(object sender, RoutedEventArgs e)
        {

        }

        private void Click_EffacerCreation(object sender, RoutedEventArgs e)
        {

        }
    }
}
