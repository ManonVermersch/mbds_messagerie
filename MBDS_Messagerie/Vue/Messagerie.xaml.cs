﻿using MBDS_Messagerie.Modele;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MBDS_Messagerie.Vue
{
    /// <summary>
    /// Logique d'interaction pour Messagerie.xaml
    /// </summary>
    public partial class Messagerie : Window
    {
        public Messagerie()
        {
            InitializeComponent();
        }

        void Supprimer_Click(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            b.Foreground = Brushes.Blue;
        }

        void Ajouter_Click(object sender, RoutedEventArgs e)
        {
            MainWindow formulaireAjout = new MainWindow();
            formulaireAjout.Show();
        }

        void Send_Click(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            b.Foreground = Brushes.BlueViolet;
        }

        public ObservableCollection<Personne> ListeDesPersonnes
        {
            get { return ListeDesPersonnes; }
            set { ListeDesPersonnes = value; }
        }
    }
}
