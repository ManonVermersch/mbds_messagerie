﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MBDS_Messagerie.Modele
{
    [Table("MessageConversation")]
    class Message
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [Column("To")]
        public string EnvoyeA { get; set; }
        [Column("From")]
        public string RecuDe { get; set; }
        [Column("Content")]
        public string Contenu { get; set; }
        [Column("SendDate")]
        public string DateEnvoie { get; set; }
    }
}
