﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MBDS_Messagerie.Modele
{
    [Table("People")]
    public class Personne
    {
        [PrimaryKey, AutoIncrement]
        public int IdPersonne { get; set; }
        [Column("Nickname")]
        public string Pseudo { get; set; }
        [Column("LastSeen")]
        public string DerniereConnexion { get; set; }
        [Column("Password")]
        public string MotDePasse { get; set; }
    }
}
