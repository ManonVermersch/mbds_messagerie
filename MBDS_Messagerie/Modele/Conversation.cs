﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MBDS_Messagerie.Modele
{
    [Table("Discussion")]
    class Conversation
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [Column("CreatorDiscussion")]
        public string CreateurConversation { get; set; }
        [Column("OtherDiscussion")]
        public string AutreConversation { get; set; }
        [Column("PrivateKey")]
        public string ClefPrivee { get; set; }
        [Column("PublicKey")]
        public string ClefPublique { get; set; }
        [Column("SymmetricalKey")]
        public string ClefSymetrique { get; set; }

    }
}
