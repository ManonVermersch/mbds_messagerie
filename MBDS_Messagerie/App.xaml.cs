﻿using MBDS_Messagerie.Vue;
using SQLite;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace MBDS_Messagerie
{
    /// <summary>
    /// Logique d'interaction pour App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            SQLiteConnection connection = new SQLiteConnection("myDb.db3");
            connection.CreateTable<Modele.Conversation>();
            connection.CreateTable<Modele.Personne>();
            connection.CreateTable<Modele.Message>();

            base.OnStartup(e);
            var app = new Connexion();
            
            app.Show();
        }
    }
}
